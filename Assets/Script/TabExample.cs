﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TabExample : MonoBehaviour {
	public string stringVar1;
	public string stringVar2;

	public int intVar1;
	public int intVar2;

//	[SerializeField]
//	[HideInInspector]
	public int toolbarTop;
	public int toolbarBottom;
	public string currentTab;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}
}
