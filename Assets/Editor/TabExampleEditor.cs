﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TabExample))]
public class TabExampleEditor : Editor {
	TabExample myTarget;
//	SerializedObject soTarget;

	SerializedProperty stringProp1;
	SerializedProperty stringProp2;

	SerializedProperty intProp1;
	SerializedProperty intProp2;

	void OnEnable() {
		myTarget = (TabExample)target;
//		soTarget = new SerializedObject(myTarget);

		stringProp1 = serializedObject.FindProperty("stringVar1");
		stringProp2 = serializedObject.FindProperty("stringVar2");

		intProp1 = serializedObject.FindProperty("intVar1");
		intProp2 = serializedObject.FindProperty("intVar2");
	}

	public override void OnInspectorGUI() {
//		base.OnInspectorGUI();
//		DrawDefaultInspector();

		serializedObject.Update();
		EditorGUI.BeginChangeCheck();

		myTarget.toolbarTop = GUILayout.Toolbar(myTarget.toolbarTop, new string[] {
			"Strings",
			"Integers",
			"Tab3",
			"Tab4"
		});

		switch (myTarget.toolbarTop) {
			case 0:
				myTarget.toolbarBottom = 4;
				myTarget.currentTab = "str";
				break;
			case 1:
				myTarget.toolbarBottom = 4;
				myTarget.currentTab = "int";
				break;
			case 2:
				myTarget.toolbarBottom = 4;
				myTarget.currentTab = "t3";
				break;
			case 3:
				myTarget.toolbarBottom = 4;
				myTarget.currentTab = "t4";
				break;
		}

		myTarget.toolbarBottom = GUILayout.Toolbar(myTarget.toolbarBottom, new string[] {
			"Tab5",
			"Tab6",
			"Tab7",
			"Tab8"
		});

		switch (myTarget.toolbarBottom) {
			case 0:
				myTarget.toolbarTop = 4;
				myTarget.currentTab = "t5";
				break;
			case 1:
				myTarget.toolbarTop = 4;
				myTarget.currentTab = "t6";
				break;
			case 2:
				myTarget.toolbarTop = 4;
				myTarget.currentTab = "t7";
				break;
			case 3:
				myTarget.toolbarTop = 4;
				myTarget.currentTab = "t8";
				break;
		}

		if (EditorGUI.EndChangeCheck()) {
			serializedObject.ApplyModifiedProperties();
			GUI.FocusControl(null);
		}

		EditorGUI.BeginChangeCheck();

		switch (myTarget.currentTab) {
			case "str":
				EditorGUILayout.PropertyField(stringProp1);
				EditorGUILayout.PropertyField(stringProp2);
				break;
			case "int":
				EditorGUILayout.PropertyField(intProp1);
				EditorGUILayout.PropertyField(intProp2);
				break;
			case "t3":
				break;
			case "t4":
				break;
		}

		if (EditorGUI.EndChangeCheck()) {
			serializedObject.ApplyModifiedProperties();
		}
	}
}
